Height := 10
Width := Height*GoldenRatio
arc1 := Circle[{Height, 0}, Height, {Pi/2, Pi}]
arc2 := Circle[{Height, Height*2 - Width}, Width - Height, {0, Pi/2}]
arc3 := Circle[{Width*2 - Height*2, Height*2 - Width}, 
  Height*2 - Width, {-Pi/2, 0}]
arc[n] := Circle[{}, , {}]
Graphics[{arc1, arc2, arc3}, AspectRatio -> Height/Width, 
 Frame -> True, PlotRange -> {{0, Width}, {0, Height}}]

arc := {x[n] ==}
RecurrenceTable[arc /. x1 -> h, y1 -> 0, r1 -> h, i1 -> Pi/2, 
 j1 -> Pi, Circle[{x, y}, r, {i, j}], {n, 1, 10}]